//event with page load for current city

window.addEventListener('load', () => {
    let long;
    let lat;
    let temperatureDesc = document.querySelector('.temperature-description');
    let location = document.querySelector('#location-timezone');
    let summaryDesc = document.querySelector('.temperature-description');
    let cityTemp = document.querySelector('.temperature-degree'); 

    if(navigator.geolocation){
        navigator.geolocation.getCurrentPosition(position => {
        long = position.coords.longitude;
        lat = position.coords.latitude;
        const proxy = 'https://cors-anywhere.herokuapp.com/';
        const api = `${proxy}https://api.darksky.net/forecast/39e1088ba865bc79aaf6006ace581c43/${lat},${long}`;
   
    fetch(api)
    .then(response => {
        return response.json();
    })
    .then( data => {
        //console.log(data);
        const {temperature,summary} = data.currently;

        //set DOM elements from the API
        //document.querySelector('span').style.display = "inline";
        cityTemp.innerHTML = temperature;
        location.innerHTML = data.timezone;
        summaryDesc.innerHTML = summary;
       
    })
    });
  
    } else{
        h1.textContent = "Please enable your location";
    }
})


// Button event for city search

document.getElementById('submit').addEventListener('click', getCity);



 function getCity(){
 let unit = '&units=metric';    
 let city= document.getElementById('city').value;
 let apiKey = "&APPID=1736b6548ac5428adb4b4d397731d23b";
 let requestURL = 'http://api.openweathermap.org/data/2.5/weather?q=' + city + apiKey + unit;
 
 //check if the user entered a city
 let modal = document.getElementById('myModal');
 let span = document.getElementById('close');

 function emptyInput(){
    if(city === ""){
        modal.style.display = "block";
        span.onclick = function() {
            modal.style.display = 'none';
          }
          window.onclick = function(e) {
            if(e.target === modal) {
              modal.style.display = 'none';
            }
          }
        //alert("Please enter a city");
    } 
 }
 
 emptyInput();
 
 //API call for the city entered by user
 fetch(requestURL)
 .then(response => {
     return response.json();})
     .then(data =>{
        let myDiv = document.querySelector('.temperature-description');
        myDiv.innerHTML = data['weather'][0]['description'];
        let cityTemp = document.querySelector('.temperature-degree'); 
        cityTemp.innerHTML = data['main']['temp'];
        document.querySelector('span').style.display = "inline";
        let details = document.querySelector('#location-timezone');
        details.innerHTML = data['name'];
     }).catch(err => {
         details.innerHTML = 'Request Failed';
         console.log('Request Failed', err);
     })
 }



